package org.zopa.quote.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.zopa.quote.domain.model.Offer;

public class QuoteTest {

    private static final List<Offer> OFFERS;

    static {
        OFFERS = Arrays.asList(
                new Offer(BigDecimal.valueOf(0.060), BigDecimal.valueOf(1650)),
                new Offer(BigDecimal.valueOf(0.070), BigDecimal.valueOf(1350)),
                new Offer(BigDecimal.valueOf(0.080), BigDecimal.valueOf(480)),
                new Offer(BigDecimal.valueOf(0.090), BigDecimal.valueOf(1520)));
    }

    @Before
    public void setUp() {
        System.out.println("*****************************");
    }

    @Test
    public void shouldChooseAndCalculateWithOneLenderOffer() {
        // given
        Quote quote = new Quote(1600, OFFERS);

        // when
        quote.chooseBestOffers();
        quote.calculate();
        quote.display();

        // then
        Assert.assertTrue("Only one offer is accepted",
                quote.getAcceptedOffers().size() == 1);
        Assert.assertTrue("The rate is then 0.060", quote.getAcceptedOffers()
                .get(0).getRate().compareTo(BigDecimal.valueOf(0.060)) == 0);
        Assert.assertTrue("The amount is then 1600", quote.getAcceptedOffers()
                .get(0).getAmount().compareTo(BigDecimal.valueOf(1600)) == 0);
        Assert.assertTrue("The requested loan is satisfied",
                quote.getPendentAmount().compareTo(BigDecimal.ZERO) == 0);
        Assert.assertTrue("The quote rate is then 6.0%", quote.getQuoteRate()
                .compareTo(BigDecimal.valueOf(0.060)) == 0);
        Assert.assertTrue("The total repayment is then 1914.68",
                quote.getTotalRepayment().setScale(2, RoundingMode.DOWN)
                .compareTo(BigDecimal.valueOf(1914.68)) == 0);
    }

    @Test
    public void shouldChooseAndCalculateWithManyLenderOffer() {
        // given
        Quote quote = new Quote(3500, OFFERS);

        // when
        quote.chooseBestOffers();
        quote.calculate();
        quote.display();

        // then
        Assert.assertTrue("Four offers are accepted",
                quote.getAcceptedOffers().size() == 4);
        Assert.assertTrue("The requested loan is satisfied",
                quote.getPendentAmount().compareTo(BigDecimal.ZERO) == 0);
        Assert.assertTrue("The quote rate is then 6.7%", quote.getQuoteRate()
                .compareTo(BigDecimal.valueOf(0.067)) == 0);
        Assert.assertTrue("The total repayment is then 4292.59",
                quote.getTotalRepayment().setScale(2, RoundingMode.DOWN)
                .compareTo(BigDecimal.valueOf(4292.59)) == 0);
    }

    @Test
    public void shouldDisplayUncoveredLoan() {
        // given
        Quote quote = new Quote(8000, OFFERS);

        // when
        quote.chooseBestOffers();
        quote.calculate();
        quote.display();

        // then
        Assert.assertTrue(quote.getAcceptedOffers().size() == 4);
        Assert.assertTrue("The requested loan is unsatisfied",
                quote.getPendentAmount().compareTo(BigDecimal.ZERO) == 1);
    }

}
