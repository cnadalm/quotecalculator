package org.zopa.quote.domain.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.zopa.quote.domain.model.Offer;

public class MarketTest {

    @Test
    public void shouldValidate() {
        // given
        String filename = "src/test/resources/market.csv";

        // when
        Market.validate(filename);

        // then
        assert true;
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotValidateExtension() {
        // given
        String filename = "market.txt";

        // when
        Market.validate(filename);

        // then
        Assert.fail("An exception should be thrown");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotValidateNotExistsFile() {
        // given
        String filename = "not-exists.csv";

        // when
        Market.validate(filename);

        // then
        Assert.fail("An exception should be thrown");
    }

    @Test
    public void shouldRetrieveLenders() {
        // given
        String filename = "src/test/resources/market.csv";

        // when
        List<Offer> offers = null;
        try {
            offers = Market.retrieveLenderOffers(filename);
        } catch (IOException ex) {
            Assert.fail("Exception thrown. Detail : " + ex.getMessage());
        }

        // then
        Assert.assertNotNull(offers);
        Assert.assertEquals(7, offers.size());
        Assert.assertTrue(BigDecimal.valueOf(0.069)
                .compareTo(offers.get(0).getRate()) == 0);
        Assert.assertTrue(BigDecimal.valueOf(0.104)
                .compareTo(offers.get(offers.size() - 1).getRate()) == 0);
    }

    @Test
    public void shouldRetrieveLendersFailsoft() {
        // given
        String filename = "src/test/resources/marketFailsoft.csv";

        // when
        List<Offer> offers = null;
        try {
            offers = Market.retrieveLenderOffers(filename);
        } catch (IOException ex) {
            Assert.fail("Exception thrown. Detail : " + ex.getMessage());
        }

        // then
        Assert.assertNotNull(offers);
        Assert.assertEquals(5, offers.size());
    }

}
