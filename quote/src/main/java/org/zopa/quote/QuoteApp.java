package org.zopa.quote;

import org.zopa.quote.domain.Quote;
import org.zopa.quote.domain.service.Market;
import java.io.IOException;
import java.util.List;
import org.zopa.quote.domain.model.Offer;
import org.zopa.quote.domain.service.Loan;

/**
 * Quote application
 *
 * @author cnadal
 */
public class QuoteApp {

    private static final String GENERIC_ERROR_MSG
            = "Example usage : java -jar quote.jar [market_file] [loan_amount]";

    /**
     * Main application method
     *
     * @param args the app command line arguments
     */
    public static void main(String[] args) {
        try {
            validate(args);

            String filename = args[0];
            List<Offer> lenders = Market.retrieveLenderOffers(filename);

            Integer requestedAmount = Integer.parseInt(args[1]);
            Quote quote = new Quote(requestedAmount, lenders);
            
            quote.chooseBestOffers();
            quote.calculate();
            quote.display();
            
        } catch (IllegalArgumentException ex) {
            printErrorAndExit(ex.getMessage());
        } catch (IOException ex) {
            printErrorAndExit(String.format("IOEXception : %s", ex.getMessage()));
        }

        System.exit(0);
    }

    private static void validate(String[] args) {
        if (args.length != 2) {
            printErrorAndExit("The application take 2 arguments.");
        }
        Market.validate(args[0]);
        Loan.validate(args[1]);
    }

    private static void printErrorAndExit(String message) {
        System.err.println(GENERIC_ERROR_MSG);
        System.err.println(String.format("Error : %s", message));

        System.exit(-1);
    }
}
