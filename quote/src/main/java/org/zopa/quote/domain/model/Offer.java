package org.zopa.quote.domain.model;

import java.math.BigDecimal;

/**
 * Represents a Lender Offer.
 * 
 * Lender name could be stored too, but it is not really needed.
 *
 * @author cnadal
 */
public class Offer {

    private final BigDecimal rate;
    private final BigDecimal amount;

    public Offer(BigDecimal rate, BigDecimal amount) {
        this.rate = rate;
        this.amount = amount;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

}
