package org.zopa.quote.domain;

import org.zopa.quote.domain.model.Offer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * Represent a Quote domain model object, and provide some services.
 *
 * @author cnadal
 */
public class Quote {

    private static final String POUND_SYMBOL = "£";

    private static final byte TIMES_COMPOUNDED_YEAR = 12;
    private static final byte LOAN_MONTHS = 36;

    private final Integer requestedAmount;
    private final List<Offer> offers;
    private final List<Offer> acceptedOffers;
    private BigDecimal pendentAmount;

    private BigDecimal quoteRate;
    private BigDecimal totalRepayment;

    /**
     * Constructor
     *
     * @param requestedAmount the requested amount to satisfy
     * @param offers the lender offers
     */
    public Quote(Integer requestedAmount, List<Offer> offers) {
        super();

        this.acceptedOffers = new ArrayList<>();
        this.quoteRate = BigDecimal.ZERO;
        this.totalRepayment = BigDecimal.ZERO;

        this.requestedAmount = requestedAmount;
        this.pendentAmount = BigDecimal.valueOf(requestedAmount);
        this.offers = offers;
    }

    /**
     * Identify the less rate offers to provide the best quote
     */
    public void chooseBestOffers() {
        offers.stream()
                .filter((offer) -> (isLoanUnsatisfied()))
                .forEachOrdered((offer) -> accept(offer));
    }

    private boolean isLoanUnsatisfied() {
        return this.pendentAmount.compareTo(BigDecimal.ZERO) == 1;
    }

    private void accept(Offer offer) {
        BigDecimal partialAmount;
        if (offer.getAmount().compareTo(this.pendentAmount) >= 0) {
            partialAmount = this.pendentAmount;
            this.pendentAmount = BigDecimal.ZERO;
        } else {
            partialAmount = offer.getAmount();
            this.pendentAmount = this.pendentAmount.subtract(offer.getAmount());
        }

        this.acceptedOffers.add(new Offer(offer.getRate(), partialAmount));
    }

    /**
     * Calculate the total repayment using the (Monthly) Compound Interest
     * Formula: Amount = Principal x (1+rate/12)^36
     */
    public void calculate() {
        BigDecimal accumulatedCosts = BigDecimal.ZERO;
        BigDecimal accumulatedInterests = BigDecimal.ZERO;
        for (Offer offer : acceptedOffers) {
            BigDecimal principal = offer.getAmount();
            BigDecimal periodicRate = offer.getRate()
                    .divide(BigDecimal.valueOf(TIMES_COMPOUNDED_YEAR), 3,
                            RoundingMode.HALF_EVEN);
            BigDecimal partialRepayment = principal
                    .multiply(periodicRate.add(BigDecimal.ONE).pow(LOAN_MONTHS));
            BigDecimal totalCompoundedInterest = partialRepayment.subtract(principal);
            accumulatedInterests = accumulatedInterests.add(totalCompoundedInterest);

            accumulatedCosts = accumulatedCosts
                    .add(principal.multiply(offer.getRate()));

            this.totalRepayment = this.totalRepayment.add(partialRepayment);
        }

        if (this.totalRepayment.compareTo(BigDecimal.ZERO) == 1) {
            this.quoteRate = accumulatedCosts.
                    divide(BigDecimal.valueOf(this.requestedAmount),
                            3, RoundingMode.UP);
        }
    }

    /**
     * Display the results
     */
    public void display() {
        if (this.isLoanUnsatisfied()) {
            System.out.println("Unfortunately the system cannot "
                    + "provide a quote at this moment");
        } else {
            System.out.println(String.format("Requested amount: %s%03d",
                    POUND_SYMBOL, this.requestedAmount));

            System.out.println(String.format("Rate: %s%%",
                    this.quoteRate.multiply(BigDecimal.valueOf(100))
                    .setScale(1, RoundingMode.UP)));

            BigDecimal monthlyRepayment = this.totalRepayment.
                    divide(BigDecimal.valueOf(LOAN_MONTHS), 2, RoundingMode.DOWN);
            System.out.println(String.format("Monthly repayment: %s%s",
                    POUND_SYMBOL, monthlyRepayment.setScale(2).toPlainString()));

            System.out.println(String.format("Total repayment: %s%s",
                    POUND_SYMBOL, this.totalRepayment
                    .setScale(2, RoundingMode.DOWN).toPlainString()));
        }
    }

    protected List<Offer> getAcceptedOffers() {
        return acceptedOffers;
    }

    protected BigDecimal getQuoteRate() {
        return quoteRate;
    }

    protected BigDecimal getTotalRepayment() {
        return totalRepayment;
    }

    protected BigDecimal getPendentAmount() {
        return pendentAmount;
    }

}
