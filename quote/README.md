1) Install Maven (version >= 3.5.0)
2) Change console encoding to : UTF-8
3) Unzip the file
4) Change directory to unzipped directory
5) Run command: mvn clean install
6) Run command: java -jar target/quote-1.0-SNAPSHOT-jar-with-dependencies.jar [market_file] [loan_amount]