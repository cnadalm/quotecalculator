1) Install Maven (version >= 3.5.0)
2) Change console encoding to : UTF-8 (in UNIX systems : )
3) Unzip the file
4) Change directory to unzipped directory
5) Run command: mvn clean install
6) Run command: java -jar target/Quote-1.0-SNAPSHOT-jar-with-dependencies.jar [market_file] [loan_amount]