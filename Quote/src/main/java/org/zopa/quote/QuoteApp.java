package org.zopa.quote;

import org.zopa.quote.domain.Quote;
import org.zopa.quote.domain.service.Market;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.zopa.quote.domain.model.Offer;
import org.zopa.quote.domain.service.Loan;

/**
 * Quote application
 *
 * ALGORITHM
 *
 * 1) Validación integración
 *
 * - Validar que hay 2 argumentos OK
 *
 * -------------@ Validar fichero
 *
 * - extension fichero OK
 *
 * - existe y es un fichero OK
 *
 * - formato de linea correcto
 *
 * - tiene uno o más registros
 *
 * -------------@ Validar préstamo
 *
 * - Entre 1000 y 15000
 *
 * - Valor entero
 *
 * - Divisible por (multiple de) 100
 *
 * 2) Ordenar las filas del fichero por tasa (de menor a mayor)
 *
 * 3) Mientras ordenas, recuperar el computo global de pasta disponible
 *
 * 4) Validar si tienes pasta suficiente, sino sales
 *
 * 5) Recorrer la lista mientras no se haya alcanzado la totalidad de la
 * petición de préstamo
 *
 * 6) Crear una sublista con los elementos necesarios
 *
 * 7) Calcular el coste a partir de la sublista
 *
 * 8) Printar el resultado
 *
 * @author cnadal
 */
public class QuoteApp {

    private static final String GENERIC_ERROR_MSG
            = "Example usage : java -jar quote.jar [market_file] [loan_amount]";

    /**
     * Main application method
     * 
     * @param args the app command line arguments
     */
    public static void main(String[] args) {
        validate(args);

        Quote quote = build(args[0],
                Short.parseShort(args[1]), Locale.UK);
        
        quote.chooseOffers();
        quote.calculate();
        quote.display();
        
        System.exit(0);
    }

    private static void validate(String[] args) {
        if (args.length != 2) {
            printError("The application take 2 arguments.");
        }

        try {
            Market.validate(args[0]);
            Loan.validate(args[1]);
        } catch (IllegalArgumentException ex) {
            printError(ex.getMessage());
        }
    }

    private static Quote build(String filename, Short requestedAmount,
            Locale locale) {
        List<Offer> lenders = new ArrayList<>();
        try {
            lenders = Market.retrieveLenderOffers(filename);
        } catch (IOException ex) {
            printError(String.format("IOEXception : %s", ex.getMessage()));
        }

        return new Quote(locale, requestedAmount, lenders);
    }

    private static void printError(String message) {
        System.err.println(GENERIC_ERROR_MSG);
        System.err.println(String.format("Error : %s", message));

        System.exit(-1);
    }
}
