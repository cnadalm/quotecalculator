package org.zopa.quote.domain.service;

import org.zopa.quote.domain.model.Offer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Provide some Market services
 *
 * @author cnadal
 */
public class Market {

    private static final String FILE_EXTENSION = ".csv";

    /**
     * Validates the integrity of the market file
     *
     * @param filename path of market file
     */
    public static void validate(String filename) {
        if (!filename.endsWith(FILE_EXTENSION)) {
            String message = String.format("[market_file] extension should be %s",
                    FILE_EXTENSION);
            throw new IllegalArgumentException(message);
        }

        File file = new File(filename);
        if (!file.exists() || !file.isFile()) {
            throw new IllegalArgumentException("[market_file] provided is not valid");
        }
    }

    /**
     * Retrieve the valid lender offers from market file
     *
     * @param filename is a valid market file
     * @return a list of lender offers sorted by rate (ASC)
     * @throws java.io.IOException
     */
    public static List<Offer> retrieveLenderOffers(String filename)
            throws IOException {
        List<Offer> lenders = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line = reader.readLine(); // file header
            while (Objects.nonNull(line)) {
                String[] fields = line.trim().split(",");
                if (fields.length == 3) {
                    //String name = fields[0]; // Not useful
                    try {
                        BigDecimal rate = BigDecimal
                                .valueOf(Double.parseDouble(fields[1].trim()));
                        BigDecimal available = BigDecimal
                                .valueOf(Double.parseDouble(fields[2].trim()));
                        lenders.add(new Offer(rate, available));
                    } catch (NumberFormatException ex) {
                        /* 
                        Error while parsing the line. We could use this catch to
                        list invalid offers, then generate an error report, for ex.
                         */
                    }
                }
                line = reader.readLine();
            }
        }

        // sort the lenders list
        return lenders.stream()
                .sorted((offer1, offer2) -> offer1.getRate().compareTo(offer2.getRate()))
                .collect(Collectors.toList());
    }

}
