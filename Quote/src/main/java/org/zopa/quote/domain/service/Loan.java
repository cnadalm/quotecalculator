package org.zopa.quote.domain.service;

import org.apache.commons.lang3.StringUtils;

/**
 * Provide some Loan services
 *
 * @author cnadal
 */
public class Loan {

    private static final Short MIN_VALUE = 1000;
    private static final Short MAX_VALUE = 15000;

    /**
     * Validates the loan amount requested
     *
     * @param amount
     */
    public static void validate(String amount) {
        if (!StringUtils.isNumeric(amount)) {
            throw new IllegalArgumentException("[loan_amount] is not a valid integer");
        }

        if (amount.length() < 4 || amount.length() > 5) {
            String errMsg = String.format("[loan_amount] must be between %n and %n",
                    MIN_VALUE, MAX_VALUE);
            throw new IllegalArgumentException(errMsg);
        }
        
        short loanAmount = Short.decode(amount);
        if(loanAmount > MAX_VALUE) {
            String errMsg = String.format("[loan_amount] must be between %n and %n",
                    MIN_VALUE, MAX_VALUE);
            throw new IllegalArgumentException(errMsg);
        }

        if (loanAmount % 100 != 0) {
            throw new IllegalArgumentException("[loan_amount] must be divisible by 100");
        }
    }
}
