package org.zopa.quote.domain;

import org.zopa.quote.domain.model.Offer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Represent a Quote domain model object, and provide some services.
 *
 * @author cnadal
 */
public class Quote {

    private static final byte MONTH_LOANS = 36;

    private final Locale locale;
    private final Short requestedAmount;
    private final List<Offer> offers;
    private final List<Offer> acceptedOffers;
    private BigDecimal uncoveredAmount;

    private BigDecimal quoteRate;
    private BigDecimal totalRepayment;

    /**
     * Constructor
     *
     * @param locale the locale
     * @param requestedAmount the requested amount to satisfy
     * @param offers the lender offers
     */
    public Quote(Locale locale, Short requestedAmount, List<Offer> offers) {
        super();

        this.acceptedOffers = new ArrayList<>();
        this.quoteRate = BigDecimal.ZERO;
        this.totalRepayment = BigDecimal.ZERO;

        this.locale = locale;
        this.requestedAmount = requestedAmount;
        this.uncoveredAmount = BigDecimal.valueOf(requestedAmount);
        this.offers = offers;
    }

    /**
     * Get the requested amount
     *
     * @return the requested amount
     */
    public Short getRequestedAmount() {
        return requestedAmount;
    }

    /**
     * Identify the less rate offers to provide the best quote
     */
    public void chooseOffers() {
        offers.stream()
                .filter((offer) -> (isLoanUncovered()))
                .forEachOrdered((offer) -> accept(offer));
    }

    private boolean isLoanUncovered() {
        return this.uncoveredAmount.compareTo(BigDecimal.ZERO) == 0;
    }

    private void accept(Offer offer) {
        BigDecimal partialAmount;
        if (offer.getAmount().compareTo(this.uncoveredAmount) >= 0) {
            partialAmount = this.uncoveredAmount;
        } else {
            partialAmount = this.uncoveredAmount.subtract(offer.getAmount());
        }

        this.acceptedOffers.add(new Offer(offer.getRate(), partialAmount));
        this.uncoveredAmount = partialAmount;
    }

    /**
     * Calculate the best quote
     */
    public void calculate() {
        BigDecimal accumulatedInterests = BigDecimal.ZERO;
        for (Offer offer : acceptedOffers) {
            BigDecimal interest = offer.getAmount().multiply(offer.getRate());
            accumulatedInterests = accumulatedInterests.add(interest);

            this.totalRepayment = this.totalRepayment
                    .add(offer.getAmount())
                    .add(interest);
        }

        this.quoteRate = BigDecimal.valueOf(100)
                .multiply(accumulatedInterests)
                .divide(this.totalRepayment, 1, RoundingMode.HALF_UP);
    }

    /**
     * Display the results
     */
    public void display() {
        if (this.isLoanUncovered()) {
            System.out.println("We are sorry but we cannot provide a quote right now");
        } else {
            System.out.println(String.format("Requested amount: %s",
                    NumberFormat.getCurrencyInstance(locale)
                            .format(this.requestedAmount)));

            this.quoteRate.setScale(1, RoundingMode.HALF_UP);
            System.out.println(String.format("Rate: %s",
                    NumberFormat.getPercentInstance(locale).format(this.quoteRate)));

            BigDecimal monthlyRepayment = this.totalRepayment.
                    divide(BigDecimal.valueOf(MONTH_LOANS), 2, RoundingMode.CEILING);
            System.out.println(String.format("Monthly repayment: %s",
                    NumberFormat.getCurrencyInstance(locale)
                            .format(monthlyRepayment)));

            System.out.println(String.format("Total repayment: %s",
                    NumberFormat.getCurrencyInstance(locale)
                            .format(totalRepayment)));
        }
    }

}
