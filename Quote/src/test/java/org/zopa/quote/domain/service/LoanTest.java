package org.zopa.quote.domain.service;

import org.junit.Assert;
import org.junit.Test;

public class LoanTest {
    
    @Test
    public void shouldValidateLoanAmountBoundaries() {
        // when
        Loan.validate(String.valueOf(1000));
        
        // and when
        Loan.validate(String.valueOf(15000));
        
        // then
        assert true;
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void shouldNotValidateNotNumericAmount() {
        // when
        Loan.validate("1000F");
        
        // then
        Assert.fail("An exception should be thrown");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void shouldNotValidateNotDecimalAmount() {
        // when
        Loan.validate("100.0");
        
        // then
        Assert.fail("An exception should be thrown");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void shouldNotValidateLowLoanAmount() {
        // when
        Loan.validate(String.valueOf(500));
        
        // then
        Assert.fail("An exception should be thrown");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void shouldNotValidateWrongIncrement() {
        // when
        Loan.validate(String.valueOf(1510));
        
        // then
        Assert.fail("An exception should be thrown");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void shouldNotValidateUpLoanAmount() {
        // when
        Loan.validate(String.valueOf(15100));
        
        // then
        Assert.fail("An exception should be thrown");
    }
    
}
